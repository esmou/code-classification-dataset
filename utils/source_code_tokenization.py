import re

def preprocess_code(source_code, tokenize=True, remove_comments=False, remove_symbols=False):

    if remove_comments:
            regex_comments = r"(?s)/\*.*?\*/|(//.*?\n)"
            source_code = re.sub(regex_comments, " ", source_code)

    pat = re.compile(r"([{},;:\[\])(+\-=|&!?/*ˆ^\\<>@\"'~%])")
    source_code = pat.sub(" \\1 ", source_code)
        
    if remove_symbols:
        regex_symbols = r"[{},.;:\[\])(+\-=|&!?/*ˆ^\\<>@\"'~%]"
        source_code = re.sub(regex_symbols, " ", source_code)

    # # remove white spaces
    source_code = re.sub(r"\s+", " ", source_code, flags=re.I)
    source_code = re.sub(r"^\s+", "", source_code)
    source_code = re.sub(r"\s+$", "", source_code)

    if tokenize:
        # source_code = word_tokenize(source_code)
        source_code = source_code.split()

    return source_code

def remove_comments(source_code,regex=r"(?s)/\*.*?\*/|(//.*?\n)"):
    source_code = re.sub(regex, "", source_code)
    source_code = re.sub(r"\s+", " ", source_code, flags=re.I)
    source_code = re.sub(r"^\s+", "", source_code)
    return re.sub(r"\s+$", "", source_code)

def remove_symbols(source_code,regex=r"[{},.;:\[\])(+\-=|&!?/*ˆ^\\<>@\"'~%]"):
    source_code = re.sub(regex, "", source_code)
    source_code = re.sub(r"\s+", " ", source_code, flags=re.I)
    source_code = re.sub(r"^\s+", "", source_code)
    return re.sub(r"\s+$", "", source_code)

def tokenize(source_code):
    return source_code.split()